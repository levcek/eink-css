import URLS_DEFAULT_LIST from "../lib/urlsDefaultList.js";
import URLS_STORAGE_KEY from "../lib/urlsStorageKey.js";
import { storage } from "wxt/storage";
export default defineBackground(() => {
  browser.runtime.onInstalled.addListener(async function () {
    await storage.setItem(URLS_STORAGE_KEY, JSON.stringify(URLS_DEFAULT_LIST));
  });
});
