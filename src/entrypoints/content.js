import URLS_STORAGE_KEY from "../lib/urlsStorageKey.js";
import CSS_DEFAULT_INJECTION from "../lib/cssDefaultInjection.js";
import { storage } from "wxt/storage";

export default defineContentScript({
  matches: ["<all_urls>"],
  main() {
    let styleElement = null;
    const currentURL = window.location.href;
    function isURLMatch(currentURL, savedURL) {
      return currentURL.includes(savedURL);
    }
    function injectStyles() {
      styleElement = document.createElement("style");
      styleElement.textContent = CSS_DEFAULT_INJECTION;
      document.head.appendChild(styleElement);
    }
    function ejectStyles() {
      if (styleElement) {
        styleElement.textContent = "";
        document.head.removeChild(styleElement);
      }
    }
    const maybeInjectOrEject = (string) => {
      if (
        JSON.parse(string || "[]").some((url) => isURLMatch(currentURL, url))
      ) {
        injectStyles();
        return;
      }
      if (styleElement) {
        ejectStyles();
        return;
      }
    };
    storage.getItem(URLS_STORAGE_KEY).then(maybeInjectOrEject);
    storage.watch(URLS_STORAGE_KEY, maybeInjectOrEject);
  },
});
